-- 1. Return the customerName of the customers who are from the Philippines
SELECT customerName FROM customers WHERE country = "Philippines";

--  2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT contactLastName,contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

--  3. Return the product name and MSRP of the product named "The Titanic"
SELECT productName,MSRP FROM products WHERE productName ="The Titanic";

--  4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

--  5. Return the names of customers who have no registered state
SELECT customerName FROM customers WHERE state IS NULL;

--  6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

--  7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
SELECT customerName,country,creditLimit FROM customers WHERE country != "USA" AND creditLimit >3000;

--  8. Return the customer names of customers whose customer names don't have 'a' in them
SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";

--  9. Return the customer numbers of orders whose comments contain the string 'DHL'
SELECT orderNumber FROM orders WHERE comments LIKE "%DHL%";

--  10. Return the product lines whose text description mentions the phrase 'state of the art'
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

--  11. Return the countries of customers without duplication
SELECT country FROM customers GROUP BY country;

--  12. Return the statuses of orders without duplication
SELECT status FROM orders GROUP BY status;

--  13. Return the customer names and countries of customers whose country is USA, France, or Canada
SELECT customerName, country FROM customers WHERE country IN ("USA","France","Canada");





--  14. Return the first name, last name, and office's city of employees whose offices are in Tokyo                                 --wait
SELECT  employees.firstName, employees.lastName,offices.city
FROM employees
JOIN offices ON employees.officeCode= (SELECT officeCode from offices WHERE city="Tokyo") AND offices.city="Tokyo";

--  15. Return the customer names of customers who were served by the employee named "Leslie Thompson"
SELECT customerName FROM customers WHERE salesRepEmployeeNumber = (SELECT employeeNumber from employees WHERE lastName="Thompson" AND firstName="Leslie");



--  16. Return the product names and customer name of products ordered by "Baane Mini Imports"
SELECT customers.customerName, products.productName FROM customers JOIN orders ON orders.customerNumber = customers.customerNumber JOIN orderdetails ON orderdetails.orderNumber = orders.orderNumber JOIN products ON products.productCode = orderdetails.productCode 	WHERE customers.customerName = "Baane Mini Imports";



--  17. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country. //wrong
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers,employees,offices WHERE customers.country = offices.country;  



--  18. Return the last names and first names of employees being supervised by "Anthony Bow".
SELECT lastName, firstName FROM employees WHERE reportsTo = (SELECT employeeNumber from employees WHERE lastName="Bow" AND firstName="Anthony");




--  19. Return the product name and MSRP of the product with the highest MSRP.
SELECT productName,MAX(MSRP) FROM products;


--  20. Return the number of customers in the UK. 
SELECT count(country) 
  FROM customers 	
  WHERE country ="UK"
 GROUP by country;



--  21. Return the number of products per product line
SELECT productLine,count(productLine) 
  FROM products 	
 GROUP by productLine;



--  22. Return the number of customers served by every employee
SELECT salesRepEmployeeNumber,count(salesRepEmployeeNumber) 
  FROM customers 	
 GROUP by salesRepEmployeeNumber;



--  23. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000
SELECT productName,quantityInStock FROM products WHERE productLine ="Planes" AND quantityInStock < 1000;


